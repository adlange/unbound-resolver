# avoid alpine 3.13 or later due to this issue on armv7
# https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.13.0#time64_requirements
FROM alpine:3.12.8 AS build

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache \
	build-base=0.5-r2 \
	curl=7.79.1-r0 \
	expat-dev=2.2.9-r1 \
	expat-static=2.2.9-r1 \
	libevent-dev=2.1.11-r1 \
	libevent-static=2.1.11-r1 \
	linux-headers=5.4.5-r1 \
	openssl-dev=1.1.1l-r0 \
	openssl-libs-static=1.1.1l-r0 \
	perl=5.30.3-r0

WORKDIR /tmp/unbound

ARG UNBOUND_SOURCE=https://www.nlnetlabs.nl/downloads/unbound/unbound-
ARG UNBOUND_VERSION=1.13.2
ARG UNBOUND_SHA256=0a13b547f3b92a026b5ebd0423f54c991e5718037fd9f72445817f6a040e1a83

RUN curl -fsSL --retry 3 "${UNBOUND_SOURCE}${UNBOUND_VERSION}.tar.gz" -o unbound.tar.gz \
	&& echo "${UNBOUND_SHA256}  unbound.tar.gz" | sha256sum -c - \
	&& tar xzf unbound.tar.gz --strip 1 \
	&& ./configure \
		--disable-rpath \
		--disable-debug \
		--with-pthreads \
		--with-libevent=/usr \
		--with-libexpat=/usr \
		--with-ssl=/usr \
		--prefix=/opt/unbound \
		--with-run-dir=/var/run/unbound \
		--with-username= \
		--with-chroot-dir= \
		--enable-fully-static \
		--disable-shared \
		--enable-event-api \
		--disable-flto \
		--with-conf-file=/etc/unbound/unbound.conf \
		--with-pidfile=/var/run/unbound.pid \
		--with-rootkey-file=/etc/unbound/root.key \
	&& make -j 4 install

WORKDIR /tmp/ldns

ARG LDNS_SOURCE=https://www.nlnetlabs.nl/downloads/ldns/ldns-
ARG LDNS_VERSION=1.7.1
ARG LDNS_SHA1=d075a08972c0f573101fb4a6250471daaa53cb3e

RUN curl -fsSL --retry 3 "${LDNS_SOURCE}${LDNS_VERSION}.tar.gz" -o ldns.tar.gz \
	&& echo "${LDNS_SHA1}  ldns.tar.gz" | sha1sum -c - \
	&& tar xzf ldns.tar.gz --strip 1 \
	&& sed -e 's/@LDFLAGS@/@LDFLAGS@ -all-static/' -i Makefile.in \
	&& ./configure --prefix=/opt/ldns --with-drill --disable-shared \
	&& make -j 4 \
	&& make install

WORKDIR /var/run/unbound

RUN mv /etc/unbound/unbound.conf /etc/unbound/example.conf \
	&& strip /opt/unbound/sbin/unbound \
	&& strip /opt/ldns/bin/drill

# ----------------------------------------------------------------------------

FROM scratch

LABEL org.opencontainers.image.authors "Kyle Harding <https://klutchell.dev>"
LABEL org.opencontainers.image.url "https://gitlab.com/klutchell/unbound"
LABEL org.opencontainers.image.documentation "https://gitlab.com/klutchell/unbound"
LABEL org.opencontainers.image.source "https://gitlab.com/klutchell/unbound"
LABEL org.opencontainers.image.title "klutchell/unbound"
LABEL org.opencontainers.image.description "Unbound is a validating, recursive, caching DNS resolver"
LABEL org.opencontainers.image.version "1.13.2"

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /lib/ld-musl-*.so.1 /lib/

COPY --from=build /opt/unbound/lib/ /lib/
COPY --from=build /opt/unbound/sbin/ /sbin/

COPY --from=build /opt/ldns/bin/ /bin/
COPY --from=build /opt/ldns/lib/ /lib/

COPY --from=build --chown=nobody:nogroup /etc/unbound /etc/unbound
COPY --from=build --chown=nobody:nogroup /var/run/unbound /var/run/unbound

COPY --chown=nobody:nogroup unbound.conf root.key root.hints /etc/unbound/

USER nobody

ENTRYPOINT ["unbound", "-d"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
	CMD [ "drill", "-Qp", "53", "nlnetlabs.nl", "@127.0.0.1" ]
