# unbound multiarch docker image

[Unbound](https://unbound.net/) is a validating, recursive, and caching DNS resolver.

## Build

```bash
# build a local image
docker build . --pull -t klutchell/unbound
```

## Test

```bash
# run selftest on local image
docker run --rm -d --name unbound klutchell/unbound
docker exec unbound unbound-anchor -v
docker exec unbound drill -p 5053 sigok.verteiltesysteme.net @127.0.0.1
docker exec unbound drill -p 5053 sigfail.verteiltesysteme.net @127.0.0.1
docker stop unbound
```

## Usage

NLnet Labs documentation: <https://nlnetlabs.nl/documentation/unbound/>

_NOTE: current releases have moved the configuration path from `/opt/unbound/etc/unbound` to `/etc/unbound` and you'll
need to change your mounted volumes accordingly!_

```bash
# print general usage
docker run --rm klutchell/unbound -h

# run a recursive dns server on host port 53
docker run --name unbound -p 53:5053/tcp -p 53:5053/udp klutchell/unbound

# run unbound server with configuration mounted from a host directory
docker run --name unbound -p 53:5053/udp -v /path/to/config:/etc/unbound klutchell/unbound

# update the root trust anchor for DNSSEC validation
# assumes your existing container is named 'unbound' as in the example above
docker exec unbound unbound-anchor -v
```

The provided `unbound.conf` will provide recursive DNS with DNSSEC validation. However Unbound has many features
available, so I recommend getting familiar with the documentation and mounting your own config directory.

- <https://nlnetlabs.nl/documentation/unbound/unbound.conf/>
- <https://nlnetlabs.nl/documentation/unbound/howto-optimise/>

Please note that `chroot` and `username` configuration fields are not supported as the service is already running
as `nobody:nogroup`.

## Acknowledgments

Original software is by NLnet Labs: <https://github.com/NLnetLabs/unbound>

## Licenses

- klutchell/unbound: [MIT License](https://gitlab.com/klutchell/unbound/blob/main/LICENSE)
- unbound: [BSD 3-Clause "New" or "Revised" License](https://github.com/NLnetLabs/unbound/blob/master/LICENSE)
