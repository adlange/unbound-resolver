# Debugging

## Testing DNS resolution

Create dnsutils pod:
```
kubectl apply -f deployment/pod-dnsutils.yml
```

Open bash in dnsutils pod:
```
kubectl exec --stdin --tty dnsutils -- bash
```

Run `dig`:
```
dig @dns-resolver.inxmail-sending.svc mx rdir.inxserver.com
```